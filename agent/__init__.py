import logging

import gym
import numpy as np
import tensorflow as tf

from reils import Space
from reils.agent.explorer import BatchAgentExplorer, OneOneExplorer
from reils.agent.frames import FrameFactory
from reils_gym.gym_environment import GymEnvironment


class GymOneOneExplorer(OneOneExplorer):
	FrameFactory = None

	def __init__(self, sess, q_size, n_agents_per_thread, on_build_graph, on_start,
				 gym_env_id: str, frame_factory: FrameFactory, scope_name: str = None, *args):
		self.gym_env_id = gym_env_id
		super(GymOneOneExplorer, self).__init__(sess, frame_factory, q_size, n_agents_per_thread,
												on_build_graph, on_start,
												scope_name,
												self, sess, frame_factory, *args)
		self.sess = sess
		gym.logger.setLevel(logging.WARN)
		# TODO Find better approach to get action space
		# All to get action space :(
		env = GymEnvironment.make(self.gym_env_id)
		workerseed = int(np.random.randint(1, 10000, dtype=np.uint32))
		env.seed(workerseed)
		self.action_space = env.action_space
		self.frame_factory = FrameFactory(
			state=[(2, 1, 128), tf.float32],
			ob=(env.observation_space.shape,
				Space.as_tf_dtype(env.observation_space.dtype)),
			ac=(env.action_space.shape, Space.as_tf_dtype(env.action_space.dtype)),
			rew=((), tf.float32),
			adv=((), tf.float32),
			start=((), tf.uint8),
			vpred=((), tf.float32)
		)
		env.close()


class GymBatchAgentExplorer(BatchAgentExplorer):
	FrameFactory = None

	def __init__(self, sess, q_size, n_agents_per_thread, on_build_graph, on_start,
				 gym_env_id: str, frame_factory: FrameFactory, scope_name: str = None, *args):
		self.gym_env_id = gym_env_id
		super(GymBatchAgentExplorer, self).__init__(sess, frame_factory, q_size, n_agents_per_thread,
													on_build_graph, on_start,
													scope_name,
													self, sess, frame_factory, *args)
		self.sess = sess
		gym.logger.setLevel(logging.WARN)
		# TODO Find better approach to get action space
		# All to get action space :(
		env = GymEnvironment.make(self.gym_env_id)
		workerseed = int(np.random.randint(1, 10000, dtype=np.uint32))
		env.seed(workerseed)
		self.action_space = env.action_space
		self.frame_factory = FrameFactory(
			state=[(2, 1, 128), tf.float32],
			ob=(env.observation_space.shape,
				Space.as_tf_dtype(env.observation_space.dtype)),
			ac=(env.action_space.shape, Space.as_tf_dtype(env.action_space.dtype)),
			rew=((), tf.float32),
			adv=((), tf.float32),
			start=((), tf.uint8),
			vpred=((), tf.float32)
		)
		env.close()
