import numpy

from datapipe.queue import StagingAreaQueue
from reils import *
from reils_gym import *

ENV_ID = 'CartPole-v0'
TIME_LENGTH = 64
BATCH_SIZE = 32
EXP_SIZE = TIME_LENGTH * 10
CLIP_PARAM = 0.2
ENT_COEFF = 0.01
VAL_COEFF = 1.0
MAX_GRAD = None
GAM = 0.99
LAM = 0.95
EPSILON = 0.95
EPSILON_DECAY_TIMESTEPS = 10000.0
ITERS = 10000
LEARNING_RATE = 1e-6
PROP_DELAY = 200
HIDDEN_UNITS = 256
DEVICE = '/cpu:0'


class SimpleTFHypothesis(TFHypothesis):
	def __init__(self, sess,
				 ob_space,
				 ac_space,
				 kind='small',
				 name: str = None):
		self.kind = kind
		super(SimpleTFHypothesis, self).__init__(sess, ob_space, ac_space, name)

	def new_state_placeholder(self, batch_shape: [None, int, list] = None):
		return tf.placeholder(tf.float32, ())

	def new_state_variable(self, batch_shape: [None, int, list] = None):
		return tf.Variable(tf.constant(0, dtype=tf.float32))

	def new_state(self, batch_size: [None, int, list] = None):
		return 0

	def assign_state(self, dest_state, src_state):
		pass

	@tf_method()
	def behave(self, observation, curr_state):
		x = observation  # [?, time_length, env]
		x = tf.divide(x, [.2, 2., .2, 2.])
		x = U.clip(x, -1, 1)
		shapes_tf = tf.shape(x)
		ob_shape = list(map(int, x.get_shape()[2:]))
		x = tf.reshape(x, [-1] + ob_shape)

		with tf.variable_scope('logits'):
			logits = U.dense(x, self.pd_type.param_shape()[0], "logits")
			logits = tf.reshape(logits, (shapes_tf[0], shapes_tf[1], self.pd_type.param_shape()[0]))

		with tf.variable_scope('value_pred'):
			value_pred = U.dense(x, 1, "value", U.normc_initializer(0.1))
			value_pred = tf.reshape(value_pred, (shapes_tf[0], shapes_tf[1], 1))[:, :, 0]

		return Outs(('logits', logits), ('value_pred', value_pred), ('next_state', tf.constant(0, dtype=tf.float32)))


class Explorer(BatchAgentGymExplorer):
	def __init__(self, sess, frame_factory: FrameFactory, q_size, n_agents_per_thread, shared_hypothesis, act_op,
				 name: str = None):
		super(Explorer, self).__init__(sess, frame_factory, q_size, n_agents_per_thread, ENV_ID, name)
		self.act_op = act_op
		self.shared_hypothesis = shared_hypothesis
		self.log_dir = None
		self.explorer_scope = None

	def build_agents(self, thread_index, thread_handler):
		thread_handler.logger = Logger(self.log_dir + '/agent/' + str(thread_index))
		thread_handler.total_reward = 0
		thread_handler.count = 0
		with tf.variable_scope(self.scope.abs + '/'):
			with tf.device('/cpu:0'):
				return MonoSelectorTFAgent(self.sess, self.shared_hypothesis, act_op=self.act_op)

	def next_frame(self, thread_index, envir, agnt, ob, curr_state):
		thread_handler = self.thread_handlers[thread_index]

		action, _, value_pred, _ = agnt.act.eval(
			feed_dict={agnt.act.i.observation: [[ob]],
					   agnt.act.i.action_selection: 0 if thread_index == 0 else 1})
		next_ob, reward, is_ep_start, _ = envir.step(action[0][0])
		reward = reward - 1

		thread_handler.count += 1
		if is_ep_start:
			thread_handler.logger.logkv('total_reward', thread_handler.total_reward)
			thread_handler.logger.dumpkvs()
			if thread_index == 0:
				print("Value: %f, Predicted: %f" % (thread_handler.total_reward, value_pred))
			thread_handler.total_reward = 0
			thread_handler.count = 0
		thread_handler.total_reward += reward
		if thread_index == 0:
			envir.render()

		return self.frame_factory(state=curr_state, ac=action[0][0], adv=0, ob=ob,
								  rew=reward,
								  vpred=value_pred[0][0],
								  start=int(is_ep_start)), next_ob


class MyTopology(Topology):
	def __init__(self, sess: tf.Session, frame_factory: FrameFactory, ob_space: Space, ac_space: Space,
				 batch_size, time_length, log_dir: str = None):
		self.ob_space, self.ac_space = ob_space, ac_space
		self.batch_size, self.time_length = batch_size, time_length
		with tf.device('/cpu:0'):
			with sess.graph.as_default():
				observation = U.get_placeholder("ob", tf.float32, [None, None] + list(self.ob_space.shape))
				self.shared_hypothesis = SimpleTFHypothesis(sess, self.ob_space, self.ac_space, name='CnnHypothesis')
				self.shared_hypothesis.behave(observation,
											  self.shared_hypothesis.new_state_placeholder([None])).freeze()
				pdtype = make_pdtype(self.ac_space)
				agent = TFAgent(sess, self.shared_hypothesis,
								[LinDecayEpsilonGreedy(pdtype, EPSILON, EPSILON_DECAY_TIMESTEPS),
								 LinDecayEpsilonGreedy(pdtype, EPSILON, EPSILON_DECAY_TIMESTEPS)])

				self.act_op = agent.act(observation, tf.placeholder(tf.int8, ()))
			super(MyTopology, self).__init__(sess, frame_factory, log_dir)

	def construct_trajectory_pipe(self):
		explr = Explorer(self.sess, self.frame_factory, 1, self.batch_size, self.shared_hypothesis, self.act_op)
		explr.log_dir = self.log_dir
		return explr

	def construct_experience(self) -> Experience:
		return CircularTupledBatchExperience(EXP_SIZE, self.frame_factory.keys)

	def construct_sampler(self, exp: Experience) -> Sampler:
		sampler = CircularSequentialManySampler(exp, self.time_length, next_timeout=60)
		sampler.use_tupled_next(True)
		return sampler

	def construct_trainer(self, feed: BatchFeeder) -> TFTrainer:
		with tf.device(DEVICE):
			hypo = SimpleTFHypothesis(self.sess, self.ob_space, self.ac_space,
									  name='learning_hypothesis')
			trainer_builder = PPO(feed, hypo, self.frame_factory, max_grad_norm=MAX_GRAD,
								  type='mlp')
			trainer_builder.train_step(tf.train.AdamOptimizer)
			return trainer_builder

	def construct_feed_pipe(self, sampler: Sampler) -> BatchFeeder:
		with tf.device('/cpu:0'):
			return BatchFeeder(StagingAreaQueue, self.sess, self.frame_factory, sampler, self.batch_size,
							   gae, gam=GAM, lam=LAM)

	def construct_hypothesis_propagator(self) -> HypothesisPropagator:
		with tf.device('/cpu:0'):
			return MultiPropagator([self.shared_hypothesis], self.trainer.pi)

	def on_training(self):
		with tf.device('/cpu:0'):
			run_train_iteration = U.function([self.trainer.multiplier, self.trainer.learning_rate],
											 [self.trainer.graph.train_step,
											  [self.trainer.graph.loss,
											   self.trainer.graph.hypothesis_loss,
											   self.trainer.graph.hypothesis_entropy,
											   self.trainer.graph.value_loss]])
		learning_rate = LEARNING_RATE
		factor = 1.0
		loss_avg = numpy.array([0., 0., 0., 0.])
		start_time = datetime.datetime.now()
		loss_names = ['total_loss', 'hypothesis_loss', 'hypothesis_entropy', 'value_loss']
		for i in range(ITERS * PROP_DELAY):

			_, losses = run_train_iteration(factor, learning_rate)
			loss_avg += losses
			factor = (1 - i / ITERS)
			learning_rate = factor * learning_rate

			for name, loss in zip(loss_names, losses):
				self.logger.logkv(name, loss)

			if i % 10 == 0:
				time_delta = (datetime.datetime.now() - start_time).microseconds / 10
				print("Iteration %d,\tAvg. duration: %fus,\tLoss Avg.: " % (
					i, time_delta), loss_avg / 10)
				self.logger.logkv('avg_time', time_delta)
				self.logger.dumpkvs()
				start_time = datetime.datetime.now()
				loss_avg = numpy.array([0., 0., 0., 0.])

			if i % PROP_DELAY == 0:
				s = datetime.datetime.now()
				self.hypothesis_propagator.start_propagation()
				e = datetime.datetime.now()
				print("Hypothesis propagation duration: %fus" % (e - s).microseconds)
				self.logger.logkv('propagation', (e - s).microseconds)


#
# self.feed_pipe.stop_all()
# self.experience.reset()
# time.sleep(10)
# self.feed_pipe.start()


session = tf.Session()
ob_space, ac_space = GymEnvironment.get_spaces(ENV_ID)  # involves creating environment
factory = get_actor_critic_factory(ob_space.shape, ac_space.shape, tf.float32)

my_topology = MyTopology(session, factory, ob_space, ac_space, BATCH_SIZE, TIME_LENGTH, 'checkpoints')
print("Starting Topology")
my_topology.start()
print("Topo complete")
