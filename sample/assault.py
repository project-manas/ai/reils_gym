from unittest import TestCase

from datapipe.queue import StagingAreaQueue
from reils import *
from reils_gym import *


class AssaultExplorer(BatchAgentGymExplorer):
	"""
	Coordinates the interactions between agents and their environments.
	Supports any mapping between agent and environment.
	"""

	def __init__(self, sess, frame_factory: FrameFactory, q_size, n_agents_per_thread, shared_hypothesis, act_op,
				 name: str = None):
		super(AssaultExplorer, self).__init__(sess, frame_factory, q_size, n_agents_per_thread, 'Assault-v0', name)
		self.act_op = act_op
		self.shared_hypothesis = shared_hypothesis

	def build_agents(self, thread_index, thread_handler):
		return MonoSelectorTFAgent(self.sess, self.shared_hypothesis, act_op=self.act_op)

	def next_frame(self, thread_index, envir, agnt, ob, curr_state):
		action, _, value_pred, _ = agnt.act.eval(
			feed_dict={agnt.act.i.observation: np.expand_dims(np.expand_dims(ob, 0), 0)})
		next_ob, reward, is_ep_start, _ = envir.step(action[0])
		return self.frame_factory(state=curr_state, ac=action[0][0], adv=0, ob=ob, rew=reward,
								  vpred=value_pred[0][0],
								  start=int(is_ep_start))


class MyTopology(Topology):
	def __init__(self, sess: tf.Session, frame_factory: FrameFactory, ob_space: Space, ac_space: Space,
				 batch_size: int = 3, time_length: int = 4, log_dir: str = None):
		self.ob_space, self.ac_space = ob_space, ac_space
		self.batch_size, self.time_length = batch_size, time_length
		# TODO Create class EnvFactory instead of passing around ob_space and ac_space everywhere
		with sess.graph.as_default():
			observation = U.get_placeholder(name="ob", dtype=tf.float32,
											shape=[None, None] + list(self.ob_space.shape))
			self.shared_hypothesis = RnnTFHypothesis(sess, self.ob_space, self.ac_space, name='RnnHypothesis')
			self.shared_hypothesis.behave(observation, self.shared_hypothesis.new_state_placeholder([None])).freeze()
			agent = MonoSelectorTFAgent(sess, self.shared_hypothesis, [Stochastic(make_pdtype(self.ac_space))])
			self.act_op = agent.act(observation)
		super(MyTopology, self).__init__(sess, frame_factory, log_dir)

	def construct_trajectory_pipe(self):
		"""
		Event which constructs the class that coordinates agents exploring the environments and generation of experience.
		:return:
		"""
		return AssaultExplorer(self.sess, self.frame_factory, 16, self.batch_size, self.shared_hypothesis, self.act_op)

	def construct_experience(self) -> Experience:
		"""
		Event for construction of Experience that accumulates and manages collected experiences.
		:return:
		"""
		return CircularTupledBatchExperience(100, self.frame_factory.keys)

	def construct_sampler(self, exp: Experience) -> Sampler:
		"""
		Event for construction of sampler that samples from the collected experience for use in training.
		:param exp:
		:return:
		"""
		sampler = CircularSequentialManySampler(exp, self.time_length, next_timeout=60)
		sampler.use_tupled_next(True)
		return sampler

	def construct_trainer(self, feed: BatchFeeder) -> TFTrainer:
		"""
		Trainer is fed samples from the dataset and learns from them to update the hypothesis.
		Finally the updated hypothesis is propagated back to the agents that explore.
		:param feed:
		:return:
		"""
		hypo = RnnTFHypothesis(self.sess, self.ob_space, self.ac_space, name='learning_hypothesis')
		multiplier = tf.placeholder(tf.float32, shape=())
		learning_rate = tf.constant(1e-3, dtype=tf.float32)
		trainer_builder = PPO(feed, hypo, multiplier, learning_rate)
		trainer_builder.train_step(tf.train.AdamOptimizer)
		return trainer_builder

	def construct_feed_pipe(self, sampler: Sampler) -> BatchFeeder:
		"""
		Event for construction of the feed pipe that uses sampled experience and performs pre-processing for efficient feeding into the trainer.
		:param sampler:
		:return:
		"""
		return BatchFeeder(StagingAreaQueue, self.sess, self.frame_factory, sampler, self.batch_size, gam=0.1, lam=0.1)

	def construct_hypothesis_propagator(self) -> HypothesisPropagator:
		return MultiPropagator([self.shared_hypothesis, self.trainer.pi_old], self.trainer.pi)

	def on_training(self):
		run_train_iteration = U.function([self.trainer.multiplier],
										 [self.trainer.graph.train_step, self.trainer.graph.loss])
		_, l = run_train_iteration(0.01)
		loss_avg = l
		loss_smoothing = 0.99
		start_time = datetime.now()
		for i in range(200):
			_, l = run_train_iteration(0.01)
			loss_avg = loss_avg * loss_smoothing + (1 - loss_smoothing) * l
			if i % 10 == 0:
				print("Iteration %d,\tAvg. duration: %fs,\tRunning Loss Avg.: %f" % (
					i, (datetime.now() - start_time).seconds / (i + 1), loss_avg))
			if i % 100 == 0:
				self.hypothesis_propagator.start_propagation()


class TestTopology(TestCase):
	def test_topology(self):
		"""
		Instantiate and start an instance of the desired topology.
		:return:
		"""
		sess = tf.Session()
		ob_space, ac_space = GymEnvironment.get_spaces('Assault-v0')  # involves creating environment
		frame_factory = FrameFactory(
			state=((1, 2, 128), tf.float32),
			ob=(ob_space.shape, tf.float32),
			ac=(ac_space.shape, tf.float32),
			rew=((), tf.float32),
			adv=((), tf.float32),
			start=((), tf.uint8),
			vpred=((), tf.float32)
		)
		my_topology = MyTopology(sess, frame_factory, ob_space, ac_space, 2, 4, 'checkpoints/')
		print("Starting Topo")
		my_topology.start()
		print("Topo complete")
