import timeit

import numpy
from tensorflow.contrib.data import prefetch_to_device

from datapipe.queue import FIFOQueue
from reils import *
from reils.trainer.double_dqn import DoubleDQN
from reils_gym import *

ENV_ID = 'CartPole-v0'
BATCH_SIZE = 1024 * 1024
EXPERIENCE_SIZE = 1024 * 1024
N_EXPLORER_THREADS = 1
AGENT_ENV_PER_THREAD = 512
CLIP_PARAM = 0.2
ENT_COEFF = 0.01
VAL_COEFF = 1.0
MAX_GRAD = None
GAM = 0.99
LAM = 0.95
EPSILON = 0.95
EPSILON_DECAY_TIMESTEPS = 10000.0
ITERS = 10000
LEARNING_RATE = 1e-3
PROP_DELAY = 200
HIDDEN_UNITS = 256
DEVICE = '/GPU:0'


class SimpleTFHypothesis(TFHypothesis):
	def __init__(self, sess,
				 ob_space,
				 ac_space,
				 kind='small',
				 name: str = None):
		self.kind = kind
		super(SimpleTFHypothesis, self).__init__(sess, ob_space, ac_space, name)

	def new_state_placeholder(self, batch_shape: [None, int, list] = None):
		return tf.placeholder(tf.float32, batch_shape)

	def new_state_variable(self, batch_shape: [None, int, list] = None):
		return tf.Variable(np.zeros(batch_shape), expected_shape=batch_shape, validate_shape=True)

	def new_state(self, batch_size: [None, int, list] = 1):
		return np.zeros(batch_size)

	def assign_state(self, dest_state, src_state):
		raise NotImplementedError

	@tf_method()
	def behave(self, observation, curr_state):
		x = observation  # [batch_size, ...]
		x = tf.divide(x, [.2, 2., .2, 2.])
		x = U.clip(x, -1, 1)
		shapes_tf = tf.shape(x)
		batch_size = shapes_tf[0]

		x = tf.layers.dense(x, 16)

		with tf.variable_scope('logits'):
			logits = U.dense(x, self.pd_type.param_shape()[0], "logits")
			logits = tf.reshape(logits, (batch_size, self.pd_type.param_shape()[0]))

		with tf.variable_scope('value_pred'):
			value_pred = tf.reduce_max(logits, axis=1)

		return Outs(('logits', logits), ('value_pred', value_pred), ('next_state', tf.zeros_like(value_pred)))


class MyExplorer(BatchAgentGymExplorer):
	class Stats(object):
		def __init__(self, n_threads: int):
			self.averages = [None] * n_threads

	def __init__(self, sess, frame_factory: FrameFactory, env_id: str, ob_space: Space, ac_space: Space,
				 n_explorer_threads, q_size, n_agent_env_per_thread,
				 log_dir: str = '.', name: str = None, **kwargs):
		super(MyExplorer, self).__init__(sess, frame_factory, env_id,
										 q_size, n_explorer_threads, n_agent_env_per_thread, n_agent_env_per_thread,
										 enable_rendering=True, enable_analytics=True, enqueue_disabled=False,
										 scope_name=name, **kwargs)
		with self.sess.graph.as_default():
			with tf.device('/cpu:0'):
				observation = U.get_placeholder("ob", tf.float32, [None] + list(ob_space.shape))
				with tf.variable_scope('DoubleDQN/loss/learning', reuse=True):
					self.shared_hypothesis = SimpleTFHypothesis(self.sess, ob_space, ac_space, name='simple_hypothesis')
					self.shared_behave_op = self.shared_hypothesis.behave(
						observation, self.shared_hypothesis.new_state_variable(n_agent_env_per_thread))
		self.explorer_scope = None
		self.log_dir = log_dir
		self.debug = self.Stats(n_explorer_threads)

	@property
	def target(self):
		return self.shared_behave_op

	def setup_thread(self, thread_index, t_handler, envs, agent):
		t_handler.agent_bz = agent_bz = self.agents_batch_size
		t_handler.loggers = [Logger(self.log_dir + '/agent/' + str(agent_i)) for agent_i in range(agent_bz)]
		t_handler.is_actives, t_handler.is_resets = np.ones(agent_bz, np.int32), np.ones(agent_bz, np.int32)
		t_handler.counts = np.zeros(agent_bz, np.int32)
		t_handler.tot_rews, t_handler.val_preds = np.zeros(agent_bz, np.float32), np.zeros(agent_bz, np.float32)
		t_handler.frame_factory = self.frame_factory
		t_handler.sess = self.sess
		t_handler.debug = self.debug

	def build_agents(self, thread_index, thread_handler, num: int):
		with tf.variable_scope(self.scope.abs + '/'):
			with tf.device('/cpu:0'):
				pdtype = make_pdtype(self.shared_hypothesis.ac_space)
				agent = MonoSelectorTFAgent(self.sess, AGENT_ENV_PER_THREAD,
											self.shared_hypothesis.new_state_variable(self.agents_batch_size),
											self.shared_hypothesis,
											[LinDecayEpsilonGreedy(pdtype, EPSILON, EPSILON_DECAY_TIMESTEPS),
											 LinDecayEpsilonGreedy(pdtype, EPSILON, EPSILON_DECAY_TIMESTEPS)],
											self.shared_behave_op)
				agent.act = agent.act(self.shared_behave_op.i.observation,
									  tf.placeholder(tf.int8, AGENT_ENV_PER_THREAD))
				return agent

	@staticmethod
	def next_frame(thread_index, thread_handler, envs, agent, obs, curr_states):
		agent_bz = thread_handler.agent_bz
		is_actives = thread_handler.is_actives
		actions, _, value_preds, next_states = agent.act.eval(
			feed_dict={agent.act.i.observation: obs,
					   agent.act.i.action_selection: np.minimum(np.arange(0, agent_bz), 1)})
		# actions, _, value_preds, next_states = [0] * agent_bz, None, [0] * agent_bz, [None] * agent_bz
		next_ob, reward, is_ep_start, _ = (np.array(arr) for arr in envs.step(actions))
		is_ep_start = is_ep_start.astype(np.int32, copy=False)
		reward = reward / 100 - 1 * is_ep_start

		np.maximum(thread_handler.is_resets, is_ep_start, out=thread_handler.is_resets)

		if np.all(thread_handler.is_resets == 1):
			thread_handler.tot_rews += reward * is_actives
			thread_handler.val_preds += value_preds * is_actives
			thread_handler.counts += is_actives

			thread_handler.is_actives *= (1 - is_ep_start)

			if np.all(is_actives == 0):
				avg_total_reward = np.average(thread_handler.tot_rews)
				avg_val_pred = np.average(thread_handler.val_preds)

				averages = thread_handler.debug.averages
				if averages[thread_index] is None:
					averages[thread_index] = np.array([avg_total_reward, avg_val_pred])
				averages[thread_index] = (averages[thread_index] + np.array([avg_total_reward, avg_val_pred])) / 2

				for agent_index in range(agent_bz):
					thread_handler.loggers[agent_index].logkv('avg_total_reward', thread_handler.tot_rews[thread_index])
					thread_handler.loggers[agent_index].dumpkvs()

				if thread_index == 0 and all(average is not None for average in averages):
					all_thread_avgs = np.average(np.array(averages), 0)
					print("Value: %f, Predicted: %f" % (
						all_thread_avgs[0], all_thread_avgs[1]))
					thread_handler.debug.averages = [None] * len(averages)

				thread_handler.tot_rews.fill(0)
				thread_handler.val_preds.fill(0)
				thread_handler.is_resets.fill(0)
				thread_handler.is_actives.fill(1)

		# if thread_index == 0:
		# 	print('#', end='', flush=True)
		return thread_handler.frame_factory(ob=obs, state=curr_states,
											ac=actions, rew=reward,
											start=is_ep_start,
											next_ob=next_ob, next_state=next_states), next_ob


class MyTopology(Topology):
	def __init__(self, sess: tf.Session, frame_factory: FrameFactory, ob_space: Space, ac_space: Space,
				 batch_size, log_dir: str = None):
		self.ob_space, self.ac_space = ob_space, ac_space
		self.batch_size = batch_size
		self.log_dir = log_dir
		self.shared_hypo = None
		self.iterator = None
		super(MyTopology, self).__init__(sess, frame_factory, BATCH_SIZE, log_dir)

	def on_setup_topology(self):
		with self.sess.graph.as_default():
			with tf.device('/cpu:0'):
				dataset = self.get_experience_dataset()
				dataset = dataset.prefetch(1000000)
				dataset = dataset.shuffle(128)
				dataset = dataset.apply(prefetch_to_device(DEVICE, buffer_size=512))
				self.iterator = dataset.make_initializable_iterator()
			with tf.device(DEVICE):
				hypo = SimpleTFHypothesis(self.sess, self.ob_space, self.ac_space, name='simple_hypothesis')
				trainer = DoubleDQN(frame_factory, self.iterator, hypo, gamma=GAM)
				trainer.train_step(tf.train.AdamOptimizer(learning_rate=LEARNING_RATE))
			with tf.device('/cpu:0'):
				explorer = MyExplorer(self.sess, frame_factory, ENV_ID, ob_space, ac_space,
									  N_EXPLORER_THREADS, 1, AGENT_ENV_PER_THREAD, self.log_dir,
									  queue_builder=lambda: FIFOQueue(self.frame_factory.shapes,
																	  self.frame_factory.dtypes, 32))
				experience = CircularTupledExperience(EXPERIENCE_SIZE, self.frame_factory.keys)
				sampler = SequentialManySampler(experience, BATCH_SIZE, next_timeout=60)
				with tf.device('/cpu:0'):
					hypothesis_propagator = MultiPropagator([explorer.target], trainer.q_s1)
			return explorer, experience, sampler, dataset, trainer, hypothesis_propagator

	def on_training(self):
		with tf.device('/cpu:0'):
			run_train_iteration = U.function([self.trainer.multiplier, self.trainer.learning_rate],
											 [self.trainer.graph.train_step,
											  [self.trainer.graph.loss]])
		learning_rate = LEARNING_RATE
		factor = 1.0
		loss_avg = numpy.array([0.])
		start_time = datetime.datetime.now()
		loss_names = ['total_loss']
		next = self.iterator.get_next()
		# sample = self.sampler.__next__()
		self.sess.run(next)
		print(timeit.timeit(lambda: self.sess.run(next), number=5))
		for i in range(ITERS * PROP_DELAY):
			_, losses = run_train_iteration(factor, learning_rate)
			loss_avg += losses
			factor = (1 - i / ITERS)
			learning_rate = factor * learning_rate

			for name, loss in zip(loss_names, losses):
				self.logger.logkv(name, loss)

			if i % 10 == 0:
				time_delta = (datetime.datetime.now() - start_time).microseconds / 10
				print("Iteration %d,\tAvg. duration: %fus,\tLoss Avg.: " % (
					i, time_delta), loss_avg / 10)
				self.logger.logkv('avg_time', time_delta)
				self.logger.dumpkvs()
				start_time = datetime.datetime.now()
				loss_avg *= 0

			if i % PROP_DELAY == 0:
				s = datetime.datetime.now()
				self.hypothesis_propagator.start_propagation()
				e = datetime.datetime.now()
				print("Hypothesis propagation duration: %fus" % (e - s).microseconds)
				self.logger.logkv('propagation', (e - s).microseconds)


config = tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))
session = tf.Session(config=config)
ob_space, ac_space = GymEnvironment.get_spaces(ENV_ID)  # involves creating environment
frame_factory = QFrameFactory(ob_space, ac_space)

my_topology = MyTopology(session, frame_factory, ob_space, ac_space, BATCH_SIZE, 'checkpoints')
my_topology.start()
