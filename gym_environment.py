import random
from typing import List

import gym
from gym import spaces

from reils import BatchAgentExplorer, FrameFactory
from reils.env.base import Environment, Space, Continuous, Discrete, SpaceTuple


class GymSpace(Space):
	@staticmethod
	def from_gym_space(gym_space: gym.Space):
		if isinstance(gym_space, spaces.Discrete):
			return Discrete(gym_space.n)
		elif isinstance(gym_space, spaces.MultiBinary):
			return Discrete([2 for _ in range(gym_space.n)])
		elif isinstance(gym_space, spaces.MultiDiscrete):
			return Discrete(gym_space.nvec)
		elif isinstance(gym_space, spaces.Box):
			return Continuous(gym_space.low, gym_space.high)
		elif isinstance(gym_space, spaces.Tuple):
			return SpaceTuple([GymSpace.from_gym_space(space) for space in gym_space.spaces])
		else:
			raise NotImplementedError(
				"The provided gym space is currently not supported. Please raise an issue or consider supporting")


class GymEnvironment(Environment):
	def __init__(self, gym_env_id):
		self.gym_env = gym.make(gym_env_id)
		super(GymEnvironment, self).__init__(self.gym_env.ob_space, self.gym_env.action_space)

	def step(self, action):
		obs, reward, done, info = self.gym_env.step(action)
		return obs, reward, done

	def reset(self):
		obs = self.gym_env.reset()
		return obs

	def render(self):
		raise NotImplementedError

	def close(self):
		raise NotImplementedError

	@staticmethod
	def make(env_id):
		env = gym.make(env_id)
		return GymEnvironment.wrap_env(env)

	@classmethod
	def wrap_env(cls, env) -> Environment:
		# env = wrap_deepmind(env, clip_rewards = True, frame_stack = False)
		# noinspection PyTypeChecker
		env.ac_space = GymSpace.from_gym_space(env.action_space)
		# noinspection PyTypeChecker
		env.ob_space = GymSpace.from_gym_space(env.observation_space)
		env.get_spaces = lambda env_id: cls.get_spaces(env_id)
		# env.action_space = Space.wrap_space(env.action_space, action_shape, action_dtype)
		return env

	@classmethod
	def get_spaces(cls, env_id):
		# All to get action space :(
		env = cls.make(env_id)
		ob_space, ac_space = env.ob_space, env.ac_space
		env.close()
		return ob_space, ac_space


class BatchEnvironment(Environment):
	def __init__(self, envs: List[Environment]):
		super(BatchEnvironment, self).__init__(envs[0].ob_space, envs[0].ac_space)
		self.envs = envs

	def step(self, actions):
		return tuple(zip(*(env.step(action) for env, action in zip(self.envs, actions))))

	def reset(self):
		return [env.reset() for env in self.envs]

	def conditional_reset(self, is_ep_start, obs):
		return [self.envs[agent_index].reset() if is_ep_start[agent_index] else obs[agent_index] for
				agent_index in range(len(self.envs))]

	def render(self):
		return self.envs[0].render()

	def close(self):
		return [env.close() for env in self.envs]


class BatchAgentGymExplorer(BatchAgentExplorer):

	def __init__(self, sess, frame_factory: FrameFactory, env_id: str,
				 q_size, n_threads: int, n_agents_per_thread=1, n_envs_per_thread=1,
				 scope_name: str = None,
				 enable_rendering=False, enable_analytics: bool = False, enqueue_disabled: bool = False,
				 *args, **kwargs):
		super(BatchAgentExplorer, self).__init__(sess, frame_factory,
												 q_size, n_threads, n_agents_per_thread, n_envs_per_thread,
												 scope_name,
												 enable_rendering, enable_analytics, enqueue_disabled,
												 *args, **kwargs)
		self.env_id = env_id

	def build_envs(self, thread_index, thread_handler, num: int):
		return BatchEnvironment(
			[self.build_env(thread_index, thread_handler) for _ in range(self.env_batch_size)])

	def build_env(self, thread_index, thread_handler):
		env = GymEnvironment.make(self.env_id)
		workerseed = int(random.randint(1, 10000))
		env.seed(workerseed)
		return env

	def build_agents(self, thread_index, thread_handler, num: int):
		raise NotImplementedError

	@staticmethod
	def next_frame(self, thread_index, env, agent, ob, curr_state):
		raise NotImplementedError
